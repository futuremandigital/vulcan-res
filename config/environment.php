<?php

/*----------------------------------------------------*/
// Define your environments
/*----------------------------------------------------*/
return function()
{
    if ($_SERVER['SERVER_NAME'] === 'vulcan-res.dev')
    {
        return 'local';
    }

    return 'production';
};