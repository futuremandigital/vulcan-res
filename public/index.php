<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Add The Title Here</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<? //All of our CSS compiled ?>
	<link rel="stylesheet" href="/css/styles.min.css" type="text/css">

	<? //Moderizr & Respond.js ?>
	<script src="/js/header.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-sm-12">

			<h1>Bootstrap</h1>

			<i class="fa fa-spinner fa-spin"></i>

		</div>
	</div>
</div>

<script src="/js/app.min.js" type="application/javascript"></script>

<?  //Google Analytics: change UA-XXXXX-X to be your site's ID. ?>
<script>
	(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src='https://www.google-analytics.com/analytics.js';
		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
	ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>

</body>
</html>

