var gulp = require('gulp'),
    gutil = require('gulp-util'),
    less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    minify_css = require('gulp-minify-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    source_maps  = require('gulp-sourcemaps');

var autoPrefix_list = ['last 2 version'];
var themosis_dir = './htdocs/content/themes/vr-theme/app/assets';

gulp.task('compile-less', function() {
    //This is the file that will contain all imports
    gulp.src('./assets/less/styles.less')
        //get source maps ready
        .pipe(source_maps.init())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: autoPrefix_list,
            cascade:  true
        }))
        .pipe(concat('styles.min.css'))
        .pipe(minify_css())
        .pipe(source_maps.write('/maps'))
        .pipe(gulp.dest(themosis_dir + '/css/'));
});




gulp.task('compile-js', function() {

    return gulp.src([
        //jQuery
        './bower_components/jquery/dist/jquery.min.js',
        //Waypoints
        //'./bower_components/waypoints/lib/jquery.waypoints.min.js',
        './bower_components/waypoints/waypoints.min.js',
        //Bootstrap
        './bower_components/bootstrap/dist/js/bootstrap.min.js',

        //GreenSock Animation Platform
        './bower_components/gsap/src/minified/TweenMax.min.js',
        './bower_components/gsap/src/minified/jquery.gsap.min.js',
        './bower_components/gsap/src/minified/plugins/CSSPlugin.min.js',
        //'./bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js',
        './bower_components/gsap/src/minified/easing/EasePack.min.js',

        //ScrollMagic
        './bower_components/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',

        //ScrollMagic Plugins
        './bower_components/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
        './bower_components/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js',
        './bower_components/scrollmagic/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js',

        //Raphael
        './assets/js/raphael-min.js',

        //Mobile Detect
        './bower_components/mobile-detect/mobile-detect.min.js',

        //Additional JS in assets/js folder
        './bower_components/slick-carousel/slick/slick.min.js',

        './assets/js/**/*.js',

    ])
        .pipe(source_maps.init())
        //Combile all the JS together, this will be the name of the file
        .pipe(concat('app.min.js'))
        //Catch errors
        //.on('error', gutil.log)
        //Compress the javascript
        //.pipe(uglify())
        .pipe(source_maps.write('/maps'))
        .pipe(gulp.dest( themosis_dir + '/js/'));
});

gulp.task('compile-header-js', function() {
    return gulp.src([
        './bower_components/modernizr/modernizr.js',
        './bower_components/respond/src/respond.js'

    ])
        //Combile all the JS together, this will be the name of the file
        .pipe(concat('header.min.js'))
        //Catch errors
        .on('error', gutil.log)
        //Compress the javascript
        .pipe(uglify())
        .pipe(gulp.dest( themosis_dir + '/js/'));

})



gulp.task('copy-fonts', function() {
    gulp.src([
        './bower_components/fontawesome/fonts/**/*',
        './bower_components/bootstrap/fonts/**/*'
    ])
        .pipe(gulp.dest(themosis_dir + '/fonts'))
});

gulp.task('watch', function() {
    gulp.watch('./assets/less/**/*.less', ['compile-less']);
    gulp.watch('./assets/js/**/*.js', ['compile-js']);
});

gulp.task('default', [
    'compile-less',
    'compile-js',
    'compile-header-js',
    'watch',
    'copy-fonts']);
