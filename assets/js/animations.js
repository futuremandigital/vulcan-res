
$(function() {
    "use strict";


    //Variable for detection
    //var md = new MobileDetect(window.navigator.userAgent);
    //console.log(md.mobile());
    //console.log(md);

//Tween Animations
TweenMax.set("h2.hero-h2", {marginLeft: -200})
TweenMax.to("h2.hero-h2", 2, {marginLeft: 0,});

TweenMax.set("h3.hero-h3", {marginLeft: -400})
TweenMax.to("h3.hero-h3", 2, {marginLeft: 0,});




// build tween
var tween = TweenMax.fromTo(".down-arrow", 1,
    {marginTop: -10},
    {marginTop: 10, repeat: -1, yoyo: true, ease: Circ.easeInOut}
);

// init controller
var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

// build scenes
new ScrollMagic.Scene({triggerElement: "#parallax1"})
    .setTween("#parallax1 > div", {y: "80%", ease: Linear.easeNone})
    // .addIndicators()
    .addTo(controller);


// init controller
var controller = new ScrollMagic.Controller();

// build tween
var tween = TweenMax.fromTo(".mission-title", 1,
    {marginLeft: -500,width:270},
    {marginLeft: 0, ease: Circ.easeInOut}
);

// build scene
var scene = new ScrollMagic.Scene({triggerHook: 'onEnter', duration: 200, offset: 100})
    .setTween(tween)
    // .addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);


// build tween
var tween = TweenMax.fromTo(".mission-content", 1,
    {left: 1500},
    {left: 0, ease: Circ.easeInOut}
);

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger1",triggerHook: 'onEnter', duration: 200, offset: 100})
    .setTween(tween)
    //.addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);


// Commitments Section Stagger Scroll

// build tween
    var tween = TweenMax.staggerTo([".safe",".peop",".natu",".comm"], 1,
        {marginTop: 0}, .25
    );

// build scene
    var scene = new ScrollMagic.Scene({triggerElement: "#trigger2", duration: 500, offset: -50})
        .setTween(tween)
        // .addIndicators({name: "loop"}) // add indicators (requires plugin)
        .addTo(controller);





var tween = TweenMax.fromTo(".content-img-left", 1,
    {left: -1500, opacity:0},
    {left: 0, ease: Power3.easeOut, y: 0, opacity:1}
);

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger3", duration: 300, offset: -50})
    .setTween(tween)
    // .addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);


//var tween = TweenMax.fromTo(".content-img-right", 1,
//    {left: "100%",opacity:0},
//    {left: "50%", ease: Power3.easeOut, y: 0,opacity:1}
//);

    var tween = TweenMax.fromTo(".saftey .bkg-img", 1,
    {left: "100%",opacity:0},
    {left: "0%", ease: Power3.easeOut, y: 0,opacity:1}
);

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#trigger4", duration: 300, offset: -50})
    .setTween(tween)
    // .addIndicators({name: "loop"}) // add indicators (requires plugin)
    .addTo(controller);




    var tween = TweenMax.fromTo("#myCarousel", 1,
        {opacity:1},
        {opacity:0}
    );

// build scene
    var scene = new ScrollMagic.Scene({triggerHook: "onEnter", duration: 800})
        .setTween(tween)
        // .addIndicators({name: "loop"}) // add indicators (requires plugin)
        .addTo(controller);













// init controller
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

// build scenes
    new ScrollMagic.Scene({triggerElement: "#parallax2"})
        .setTween("#parallax2 > section", {y: "100%", ease: Linear.easeNone})
        // .addIndicators()
        .addTo(controller);



});







$(".content-img-left, .content-img-right").hover(over,out);


function over(){
    TweenMax.to(".caption", 0.5, {backgroundColor:"rgba(0,0,0,.6)",bottom:0})
}

function out(){
    TweenMax.to(".caption", 0.5, {backgroundColor:"rgba(0,0,0,0)",bottom:-100})
}


