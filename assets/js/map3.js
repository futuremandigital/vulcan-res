var svg = '';

$(window).load(function () {

    svg = $('#map').contents();

    console.log(svg);
    //$('#active_programs polygon, #active_programs path', svg).css('cursor', 'pointer');


    $(svg).on('mousemove', map_move);

    $($('#western, #international, #southwest, #mountain, #southerngulf, #southeast, #central, #mideast', svg))
        .on('mouseenter', map_enter)
        .on('mouseleave', map_leave);
});

function map_move(e) {

    var moveLeft = 0;
    var moveDown = 480;

    $(".pop-up").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
}

function map_enter(e) {

    var id = $(this).attr('id');
    $('.pop-up-' + id).show();
    TweenLite.to($(this),.25, {opacity:.5});

}

function map_leave(e) {

    var id = $(this).attr('id');
    TweenLite.to($(this),.25, {opacity:1});
    $('.pop-up-' + id).hide();
}



$(function() {

    setTimeout(fade_out, 4000);

    function fade_out() {
        $("#mapp-popup1").fadeOut().empty();
    }
});