$(function(){
    //"use strict";




    var headerAnim = new TimelineLite({paused:true});
    headerAnim.to(".navbar", 1, {background:'rgba(47, 47, 47, 0.95);',height:'82px', color:'#fff', ease:Back.easeOut});

    var header = $(".navbar");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            header.removeClass('not-sticky').addClass("sticky");
            //console.log("sticky")
            headerAnim.play()
        } else {
            header.removeClass("sticky").addClass('not-sticky');
            //console.log("notsticky")
            headerAnim.reverse()
        }
    });



    //Footer slider
    $('.slides').slick({
        //setting-name: setting-value
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '.left-arrow',
        nextArrow: '.next-arrow',
        autoplay: true,
        autoplaySpeed: 4000,
        centerMode: true,
        centerPadding: '250px',
        dots: true,
        customPaging: function(slider, i) {
            // this example would render "tabs" with titles
            return '<button class="tab">' + $(slider.$slides[i]).find('.slide-title').text() + '</button>';
        },
        responsive: [
            {
                breakpoint: 1330,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerPadding: '180px',

                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    //What Other Are Saying
    $('.other-slides').slick({
        //setting-name: setting-value
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '.left-arrow',
        nextArrow: '.next-arrow',
        dots: true,
        customPaging: function(slider, i) {
            // this example would render "tabs" with titles
            return '<button class="tab">' + $(slider.$slides[i]).find('.slide-title').text() + '</button>';
        },
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,

                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


    $('.ceo-letter').click(function(){
        $(this).find('.ceo-content').toggle();
    });



    $.fn.counterUp = function( options ) {

        // Defaults
        var settings = $.extend({
            'time': 400,
            'delay': 10
        }, options);

        return this.each(function(){

            // Store the object
            var $this = $(this);
            var $settings = settings;

            var counterUpper = function() {
                var nums = [];
                var divisions = $settings.time / $settings.delay;
                var num = $this.text();
                var isComma = /[0-9]+,[0-9]+/.test(num);
                num = num.replace(/,/g, '');
                var isInt = /^[0-9]+$/.test(num);
                var isFloat = /^[0-9]+\.[0-9]+$/.test(num);
                var decimalPlaces = isFloat ? (num.split('.')[1] || []).length : 0;


                // Generate list of incremental numbers to display
                for (var i = divisions; i >= 1; i--) {

                    // Preserve as int if input was int
                    var newNum = parseInt(num / divisions * i);

                    // Preserve float if input was float
                    if (isFloat) {
                        newNum = parseFloat(num / divisions * i).toFixed(decimalPlaces);
                    }

                    // Preserve commas if input had commas
                    if (isComma) {
                        while (/(\d+)(\d{3})/.test(newNum.toString())) {
                            newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
                        }
                    }

                    nums.unshift(newNum);
                }

                $this.data('counterup-nums', nums);
                $this.text('0');

                // Updates the number until we're done
                var f = function() {
                    $this.text($this.data('counterup-nums').shift());
                    if ($this.data('counterup-nums').length) {
                        setTimeout($this.data('counterup-func'), $settings.delay);
                    } else {
                        delete $this.data('counterup-nums');
                        $this.data('counterup-nums', null);
                        $this.data('counterup-func', null);
                    }
                };
                $this.data('counterup-func', f);

                // Start the count up
                setTimeout($this.data('counterup-func'), $settings.delay);
            };


            // Perform counts when the element gets into view
            $this.waypoint(counterUpper, { offset: '100%', triggerOnce: true });
        });

    };

    $('.counter').counterUp({
        delay: 10,
        time: 1000,
    });



});

