@extends('layouts.main')

@section('content')

<? //Hero Carousel ?>

<? //Hero Carousel ?>
<section id="slider">
    <div id="myCarousel" class="carousel slide tk-nimbus-sans-condensed" data-ride="carousel" data-interval="9000">
        <div class="carousel-inner" role="listbox">

            @foreach ( $page->carousel as $index => $item )
            <div class="item {{ ($index == 1) ? 'active' : '' }} slide-{{ $index }}" style="background-image: url('{{ $item['banner'][0] }}')">
                <div class="container">
                    <div class="carousel-caption">
                        <h2 class="hero-h2">{{ nl2br($item['slide_title']) }}</h2>
                        <h3 class="hero-h3">{{ nl2br($item['slide_subtitle']) }}</h3>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    <!--
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="arrow-left"><img src="{{ themosis_assets() }}/images/arrow-left.png" height="40"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="arrow-right"><img src="{{ themosis_assets() }}/images/arrow-right.png" height="40"></span>
            <span class="sr-only">Next</span>
        </a>
        -->
    </div>
</section>


<? //Intro ?>

<section id="intro">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="section-title text-center">{{ $page->quote_title }}</h2>
            </div>
        </div>

    </div>
</section>
<div id="trigger1" class="spacer s0"></div>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="quote-box">
                <p>{{ $page->box_content }}</p>
                <h3>“{{ $page->box_quote }}”</h3>
                <p class="text-right">-{{ $page->box_author }}</p>
            </div>
        </div>
    </div>
</div>
<div id="parallax1" class="parallaxParent">
    <div style="background-image: url('/content/uploads/2015/09/brothers-parrallax.png');">

    </div>
</div>




<!--
<section id="intro-quote" style="background-image: url('{{ $page->intro_bg_img[0] }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="quote-box">
                    <p>{{ $page->box_content }}</p>
                    <h3>“{{ $page->box_quote }}”</h3>
                    <p class="text-right">-{{ $page->box_author }}</p>
                </div>
            </div>
        </div>
    </div>
</section>
-->

<? //Mission ?>

<section id="mission">
    <div class="container">
        <div class="row">
            <div class="col-md-3 gray">
                <h2 class="mission-title text-right">{{ $page->mission_title }}</h2>
            </div>
            <div class="col-md-9">
                <h4 class="mission-content text-left">{{ $page->mission_content }}</h4>
            </div>
        </div>
    </div>
</section>

<? //Guiding Principles ?>

<section id="principles">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2 class="section-title green-text">{{ $page->principles_title }}</h2>
            </div>
        </div>
        <div class="row">
        <div class="left-arrow"><i class="fa fa-angle-left fa-2"></i></div>
        <div class="next-arrow"><i class="fa fa-angle-right fa-2"></i></div>
            <div class="col-md-12">
                <div class="row slides responsive">
                    @foreach ( $page->principles_slider as $item )
                    <div><img src="{{ $item['pslide_bg'][0] }}">
                        <div class="principles-content">
                            <h2 class="green-text slide-title">{{ nl2br($item['pslide_title']) }}</h2>
                            <h3>{{ nl2br($item['pslide_subtitle']) }}</h3>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<? //CEO ?>

<section id="ceo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2 class="section-title wide-title text-center">{{ $page->ceo_title }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="ceo-bg">
                <fieldset class="ceo-letter">
                    <div class="ceo-intro">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>{{ $page->ceo_preview }}</p>
                                </div>
                                <div class="col-md-6">
                                    <button class="ceo-button">{{ $page->ceo_button }} <i class="fa fa-angle-up"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ceo-content" style="display:none;">
                        <div class="container">

                            <div class="row">
                                <div class="x-close green-text"><i class="fa fa-angle-down"></i></div>
                                <div class="col-md-12">
                                    {{ $page->ceo_content }}
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</section>

<? //Commitments ?>

<section id="commitments">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2 class="section-title green-text wide-title">{{ $page->commitments_title }}</h2>
            </div>
        </div>
        <div class="row">
            @foreach ( $page->commitment as $item )
            <div class="col-md-3 commit-img {{ $item['commitment_class'] }}">
                <img src="{{ $item['commitment_img'][0] }}" width="100%">
                <div class="commit-title" style="background:{{ $item['commitment_color'] }}">
                    <div class="center-title">
                <span class="text-center">{{ $item['commitment_title1'] }}</span>
                    </div>
                </div>
                <p>{{ nl2br($item['commitment_content1']) }}</p>
                <div class="commitment-button"><a href="{{ $item['commitment_button_link1'] }}">{{ $item['commitment_button_title1'] }}</a></div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<div class="commitment-tab-section">
    <div class="header"><span>{{ $item['commitment_title1'] }} +</span>

    </div>
    <div class="content">
        <div class="col-md-3 commit-img {{ $item['commitment_class'] }}">
            <img src="{{ $item['commitment_img'][0] }}" width="100%">
            <div class="commit-title" style="background:{{ $item['commitment_color'] }}">
                <div class="center-title">
                    <span class="text-center">{{ $item['commitment_title1'] }}</span>
                </div>
            </div>
            <p>{{ nl2br($item['commitment_content1']) }}</p>
            <div class="commitment-button"><a href="{{ $item['commitment_button_link1'] }}">{{ $item['commitment_button_title1'] }}</a></div>
        </div>
    </div>
</div>
<div class="commitment-tab-section">
    <div class="header"><span>{{ $item['commitment_title1'] }} +</span>

    </div>
    <div class="content">
        <ul>
            <li>This is just some random content.</li>
            <li>This is just some random content.</li>
            <li>This is just some random content.</li>
            <li>This is just some random content.</li>
        </ul>
    </div>
</div>






@stop
