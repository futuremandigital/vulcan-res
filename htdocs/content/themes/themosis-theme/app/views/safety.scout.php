@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->safety1_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->safety1_content }}
                </div>
            </div>
        </div>
    </div>
<!--
<section id="mainwrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6" >
                <div id="box-1" class="box">
                    <img id="image-1" src="http://vulcan-res.dev/content/uploads/2015/09/safety-1.jpg" width="100%">
        <span class="caption simple-caption">
        <p>Simple Caption</p>
        </span>
                </div>
            </div>

        </div>
    </div>
</section>
-->







<section id="stat-section">
    <div class="container">
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>MSHA Reportable Injury Rate</h4>
            <img src="/content/uploads/2015/09/bar-chart-icon.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-1.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>MSHA Citation Rate</h4>
            <img src="/content/uploads/2015/09/bar-chart-icon.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-2.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>MSHA/OSHA Recordable Injury Rate</h4>
            <img src="/content/uploads/2015/09/line-chart.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-3.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
    </div>
</section>

<section id="content-section-left">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->safety2_content }}
                </div>
            </div>
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->safety2_image[0] }}')" >
            </div>
        </div>
    </div>
</section>

<section id="stat-section">
    <div class="container">
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>employee participation in occupation health testing</h4>
            <img src="/content/uploads/2015/09/pie-chart.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-4.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>MSHA Respirable Dust/Silica Exposure Sampling</h4>
            <img src="/content/uploads/2015/09/bar-chart-icon.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-5.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal">
            <h4>MSHA Respirable Noise Exposure Sampling</h4>
            <img src="/content/uploads/2015/09/bar-chart-icon.png" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="/content/uploads/2015/09/chart-6.png" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
    </div>
</section>

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->safety3_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->safety3_content }}
                </div>
            </div>
        </div>
    </div>
</section>



@stop