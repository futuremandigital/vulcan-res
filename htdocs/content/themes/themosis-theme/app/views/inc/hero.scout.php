<section id="subpage-hero" style="background-image: url('{{ $page->hero_background[0] }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3>{{ $page->hero_title }}</h3>
                <div class="quote-box">
                    {{ $page->hero_statement }}
                </div>
            </div>
        </div>
    </div>
</section>




