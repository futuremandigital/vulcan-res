@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->people1_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->people1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/employees-icon.png" width="70">
            <span class="counter">8,000</span>
            <h4>EMPLOYEES</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/tenure-icon.png" width="70">
            <span class="counter">14</span>
            <h4>YEARS AVERAGE TENURE</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/hand-shake-icon.png" width="70">
            <span class="counter">800</span>
            <h4>NEW HIRES IN 2014</h4>
        </div>
    </div>
</section>



@stop

