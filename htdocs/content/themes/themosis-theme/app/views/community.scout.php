@extends('layouts.main')

@section('content')

<section id="subpage-hero" style="background-image: url('{{ $page->hero_background[0] }}')">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-5">
                <h3>{{ $page->hero_title }}</h3>
                <div class="quote-box">
                    {{ $page->hero_statement }}
                </div>
            </div>
        </div>
    </div>
</section>

@include('inc/intro-quote')

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->community1_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->community1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 col-md-offset-2 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/employees-icon.png" width="70">
            <span class="counter">114</span>
            <h4>SCHOLARSHIPS AWARDED<br>IN 2014</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/money-icon.png" width="70">
            $<span class="counter">424,000</span>
            <h4>SCHOLARSHIP FUNDS<br>AWARDED IN 2014</h4>
        </div>

    </div>
</section>

<section id="content-section-left">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->community2_content }}
                </div>
            </div>
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->community2_image[0] }}')" >
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/house-icon.png" width="70">
            <span class="counter">276</span>
            <h4>SCHOOL PARTNERSHIPS</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/people-icon.png" width="70">
            <span class="counter">29,132</span>
            <h4>AVERAGE ANNUAL VISITORS</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/man-flag-icon.png" width="70">
            <span class="counter">630</span>
            <h4>AVERAGE ANNUAL TOURS</h4>
        </div>
    </div>
</section>

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->community3_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->community3_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/money-hand-icon.png" width="70">
            $<span class="counter">19.8</span>M
            <h4>DONATED OVER YEARS</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/hands-icon.png" width="70">
            <span class="counter">3,339</span>
            <h4>FOUNDATION AND MATCHING GIFT RECIPIENTS OVER 5 YEARS</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/money-plus-icon.png" width="70">
            $<span class="counter">2.3</span>M
            <h4>MATCHING GIFTS FOR EMPLOYEE CHARITABLE CONTRIBUTIONS OVER 5 YEARS</h4>
        </div>
    </div>
</section>






@stop