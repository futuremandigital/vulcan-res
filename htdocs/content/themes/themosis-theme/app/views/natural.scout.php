@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->natural1_image[0] }}')" >
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->natural1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/wheat-icon.png" width="70">
            <span class="counter">210,000</span>
            <h4>ACRES OF LAND IN OuR PORTFOLIO</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/water-icon.png" width="70">
            <span class="counter">2.5</span>B
            <h4>GALLONS OF WATER TO BE STORED AT FORMER ATLANTA QUARRY</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/deer-icon.png" width="70">
            <span class="counter">42</span>
            <h4>WILDLIFE HABITAT COUNCIL CERTIFIED SITES</h4>
        </div>
    </div>
</section>

<section id="content-section-left">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->natural2_content }}
                </div>
            </div>
            <div class="col-md-6 content-img" style="background-image: url('{{ $page->natural2_image[0] }}')" >
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 col-md-offset-2 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/gas-icon.png" width="70">
            <span class="counter">265,000</span>
            <h4>GALLONS OF USED OIL RECYCLED IN 2014</h4>
        </div>

        <div class="col-md-4 text-center view-chart">
            <img src="http://vulcan-res.dev/content/uploads/2015/09/recycle-icon.png" width="70">
            <span class="counter">1.6</span>M
            <h4>TONS OF RECYCLABLE AGGREGATES PRODUCTS PRODUCED IN 2014</h4>
        </div>

    </div>
</section>





@stop