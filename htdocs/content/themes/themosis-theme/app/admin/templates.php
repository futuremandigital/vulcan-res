<?

//Check for templates and create custom Meta for editing
if (!empty($_GET['post']))
{
    $template = Meta::get($_GET['post'], '_themosisPageTemplate');
}

//Save template metabox data
if (!empty($_POST['post_ID']))
{
    $template = Meta::get($_POST['post_ID'], '_themosisPageTemplate');
}

//Meta for every page except homepage
if (!empty($template))
{

    if ($template != 'homepage')
    {

        Metabox::make('Hero Element', 'page', [
            'context' => 'normal'
        ])->set([
            Field::media('hero-banner', ['title' => 'Hero Background']),
            Field::text('hero-title', ['title' => 'Hero Title']),
            Field::editor('hero-statement', ['title' => 'Hero Statement'])

        ]);

        Metabox::make('Header Content', 'page', [
            'context' => 'normal'
        ])->set([
            Field::text('intro-quote', ['title' => 'Quote']),
            Field::text('quote-author', ['title' => 'Author'])
        ]);


    }

    switch ($template){
        case 'homepage':

            Metabox::make('Hero Carousel', 'page', [
                'context'   => 'normal'
            ])->set(array(

                Field::infinite('carousel', [
                    Field::media('banner', ['title' => 'Slide Background']),
                    Field::text('slide_title', ['title' => 'Slide Title']),
                    Field::text('slide_subtitle', ['title' => 'Slide Sub Title']),
                ], [
                    'title' => 'Homepage Carousel',
                    'limit' => 10
                ])
            ));

            Metabox::make('Intro', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('intro_bg_img', ['title' => 'Background Image']),
                Field::text('quote_title', ['title' => 'Quote Title']),
                Field::editor('box_content', ['title' => 'Box Content']),
                Field::text('box_quote', ['title' => 'Box Quote']),
                Field::text('box_author', ['title' => 'Box Author']),
            ]);

            Metabox::make('Our Mission', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('mission_title', ['title' => 'Title']),
                Field::textarea('mission_content', ['title' => 'Content']),
            ]);


            Metabox::make('Guiding Principles', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('principles_title', ['title' => 'Title']),
                Field::infinite('principles_slider', [
                    Field::media('pslide_bg', ['title' => 'Slide Background']),
                    Field::text('pslide_title', ['title' => 'Slide Title']),
                    Field::textarea('pslide_subtitle', ['title' => 'Slide Sub Title']),
                ], [
                    'title' => 'Principle Slider',
                    'limit' => 10
                ])
            ]);


            Metabox::make('Letter from CEO', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('ceo_title', ['title' => 'Title']),
                Field::editor('ceo_preview', ['title' => 'Preview Text']),
                Field::editor('ceo_content', ['title' => 'Full Text']),
                Field::text('ceo_button', ['title' => 'Button Title']),
            ]);




            Metabox::make('Commitments', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::textarea('commitments_title', ['title' => 'Title']),
                Field::infinite('commitment',[
                    Field::media('commitment_img', ['title' => 'Image']),
                    Field::text('commitment_title1', ['title' => 'Title']),
                    Field::text('commitment_color', ['title' => 'Background Color']),
                    Field::text('commitment_class', ['title' => 'Section Class']),
                    Field::textarea('commitment_content1', ['title' => 'Content']),
                    Field::text('commitment_button_title1', ['title' => 'Button Title']),
                    Field::text('commitment_button_link1', ['title' => 'Button Link']),
                ], [
                    'title' => 'Commitment',
                    'limit' => 4
                ])
            ]);

        break;

        case 'safety';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety1-image', ['title' => 'Image']),
                Field::editor('safety1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety2-image', ['title' => 'Image']),
                Field::editor('safety2-content', ['title' => 'Content']),
            ]);

            Metabox::make('Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety3-image', ['title' => 'Image']),
                Field::editor('safety3-content', ['title' => 'Content']),
            ]);

        break;

        case 'people';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('people1-image', ['title' => 'Image']),
                Field::editor('people1-content', ['title' => 'Content']),
            ]);

        break;

        case 'natural';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural1-image', ['title' => 'Image']),
                Field::editor('natural1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural2-image', ['title' => 'Image']),
                Field::editor('natural2-content', ['title' => 'Content']),
            ]);

        break;

        case 'community';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community1-image', ['title' => 'Image']),
                Field::editor('community1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community2-image', ['title' => 'Image']),
                Field::editor('community2-content', ['title' => 'Content']),
            ]);

            Metabox::make('Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community3-image', ['title' => 'Image']),
                Field::editor('community3-content', ['title' => 'Content']),
            ]);

    }

}