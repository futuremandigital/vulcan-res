<?php

/*
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('template', ['homepage', 'uses' => 'PagesController@homepage']);
Route::get('template', ['safety', 'uses' => 'PagesController@safety']);
Route::get('template', ['people', 'uses' => 'PagesController@people']);
Route::get('template', ['natural', 'uses' => 'PagesController@natural']);
Route::get('template', ['community', 'uses' => 'PagesController@community']);




