<?php

class PagesController extends BaseController
{

    public function __construct()
    {
        // Do something global here
    }


    //Homepage
    public function homepage()
    {

        $post_id = get_the_ID();

        $page = new stdClass();
        $carousel = Meta::get($post_id, 'carousel');

        foreach ($carousel as &$c) {
            $c['banner'] = wp_get_attachment_image_src($c['banner'], 'full');

        }

        $page->carousel = $carousel;

        // Intro
        $page->intro_bg_img = wp_get_attachment_image_src(Meta::get($post_id, 'intro_bg_img'), 'full');
        $page->quote_title = Meta::get($post_id, 'quote_title');
        $page->box_content = nl2br(Meta::get($post_id, 'box_content'));
        $page->box_quote = Meta::get($post_id, 'box_quote');
        $page->box_author = Meta::get($post_id, 'box_author');


        // Mission

        $page->mission_title = Meta::get($post_id, 'mission_title');
        $page->mission_content = nl2br(Meta::get($post_id, 'mission_content'));


        // Guiding Principles

        $page->principles_title = Meta::get($post_id, 'principles_title');

        $principles_slider = Meta::get($post_id, 'principles_slider');
        foreach ($principles_slider as &$p) {
            $p['pslide_bg'] = wp_get_attachment_image_src($p['pslide_bg'], 'full');
        }
        $page->principles_slider = $principles_slider;


        // Letter From the CEO

        $page->ceo_title = Meta::get($post_id, 'ceo_title');
        $page->ceo_preview = nl2br(Meta::get($post_id, 'ceo_preview'));
        $page->ceo_content = apply_filters('the_content', Meta::get($post_id, 'ceo_content'));
        $page->ceo_button = Meta::get($post_id, 'ceo_button');


        // Commitments

        $page->commitments_title = nl2br(Meta::get($post_id, 'commitments_title'));

        $commitment = Meta::get($post_id, 'commitment');
        foreach ($commitment as &$t) {
            $t['commitment_img'] = wp_get_attachment_image_src($t['commitment_img'], 'full');
            $t['commitment_title'] = $t['commitment_title1'];
            $t['commitment_color'] = $t['commitment_color'];
            $t['commitment_class'] = $t['commitment_class'];
            $t['commitment_content'] = wp_get_attachment_image_src(Meta::get($post_id, 'commitment_content1'), 'full');
            $t['commitment_button_title'] = wp_get_attachment_image_src(Meta::get($post_id, 'commitment_button_title1'), 'full');
            $t['commitment_button_link'] = wp_get_attachment_image_src(Meta::get($post_id, 'commitment_button_link1'), 'full');
        }
        $page->commitment = $commitment;

        return View::make('homepage', compact('page'));
    }





    //Safety
    public function safety()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->safety1_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety1-image'), 'full');
        $page->safety1_content = apply_filters('the_content', Meta::get($post_id, 'safety1-content'));
        $page->safety2_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety2-image'), 'full');
        $page->safety2_content = apply_filters('the_content', Meta::get($post_id, 'safety2-content'));
        $page->safety3_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety3-image'), 'full');
        $page->safety3_content = apply_filters('the_content', Meta::get($post_id, 'safety3-content'));


        return View::make('safety', compact('page'));
    }

    //People
    public function people()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->people1_image = wp_get_attachment_image_src(Meta::get($post_id, 'people1-image'), 'full');
        $page->people1_content = apply_filters('the_content', Meta::get($post_id, 'people1-content'));

        return View::make('people', compact('page'));
    }

    //Natural
    public function natural()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->natural1_image = wp_get_attachment_image_src(Meta::get($post_id, 'natural1-image'), 'full');
        $page->natural1_content = apply_filters('the_content', Meta::get($post_id, 'natural1-content'));
        $page->natural2_image = wp_get_attachment_image_src(Meta::get($post_id, 'natural2-image'), 'full');
        $page->natural2_content = apply_filters('the_content', Meta::get($post_id, 'natural2-content'));

        return View::make('natural', compact('page'));
    }

    //Community
    public function community()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->community1_image = wp_get_attachment_image_src(Meta::get($post_id, 'community1-image'), 'full');
        $page->community1_content = apply_filters('the_content', Meta::get($post_id, 'community1-content'));
        $page->community2_image = wp_get_attachment_image_src(Meta::get($post_id, 'community2-image'), 'full');
        $page->community2_content = apply_filters('the_content', Meta::get($post_id, 'community2-content'));
        $page->community3_image = wp_get_attachment_image_src(Meta::get($post_id, 'community3-image'), 'full');
        $page->community3_content = apply_filters('the_content', Meta::get($post_id, 'community3-content'));

        return View::make('community', compact('page'));
    }

    private function get_header($post_id)
    {


        $header = new stdClass();
        $header->hero_background = wp_get_attachment_image_src(Meta::get($post_id, 'hero-banner'), 'full');
        $header->hero_title = nl2br(Meta::get($post_id, 'hero-title'));
        $header->hero_statement = nl2br(Meta::get($post_id, 'hero-statement'));

        $header->intro_quote = nl2br(Meta::get($post_id, 'intro-quote'));
        $header->quote_author = nl2br(Meta::get($post_id, 'quote-author'));

        return $header;
    }




}