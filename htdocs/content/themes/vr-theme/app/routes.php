<?php

/*
 * Define your routes and which views to display
 * depending of the query.
 *
 * Based on WordPress conditional tags from the WordPress Codex
 * http://codex.wordpress.org/Conditional_Tags
 *
 */

Route::get('template', ['homepage', 'uses' => 'PagesController@homepage']);
Route::get('template', ['safety', 'uses' => 'PagesController@safety']);
Route::get('template', ['people', 'uses' => 'PagesController@people']);
Route::get('template', ['natural', 'uses' => 'PagesController@natural']);
Route::get('template', ['community', 'uses' => 'PagesController@community']);
Route::get('template', ['others', 'uses' => 'PagesController@others']);
Route::get('template', ['map', 'uses' => 'PagesController@map']);


Route::get('singular', ['uses' => 'PostsController@single']);
Route::get('category', ['uses' => 'PostsController@category']);


//Global Variable commitments
$post_id = 5;
$commitments = new stdClass();
$commitments->commitments_title = nl2br(Meta::get($post_id, 'commitments_title'));

$commitment = Meta::get($post_id, 'commitment');
foreach ($commitment as &$t) {
    $t['commitment_img'] = wp_get_attachment_image_src($t['commitment_img'], 'full');
    $t['commitment_title'] = $t['commitment_title1'];
    $t['commitment_color'] = $t['commitment_color'];
    $t['commitment_class'] = $t['commitment_class'];
    $t['commitment_content'] = Meta::get($post_id, 'commitment_content1');
    $t['commitment_button_title'] = Meta::get($post_id, 'commitment_button_title1');
    $t['commitment_button_link'] = Meta::get($post_id, 'commitment_button_link1');
}
$commitments->commitment = $commitment;

View::share(['commitments' => $commitments]);