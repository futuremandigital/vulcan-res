@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')


<div id="trigger3" class="spacer s0"></div>
<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->safety1_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->safety1_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->safety1_content }}
                </div>
            </div>
        </div>
    </div>
</section>
<!--
<section id="mainwrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6" >
                <div id="box-1" class="box">
                    <img id="image-1" src="http://vulcan-res.dev/content/uploads/2015/09/safety-1.jpg" width="100%">
        <span class="caption simple-caption">
        <p>Simple Caption</p>
        </span>
                </div>
            </div>

        </div>
    </div>
</section>
-->







<section id="stat-section">
    <div class="container">
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-1">
            <h4>{{ $page->safety_stat1_1_text }}</h4>
            <img src="{{ $page->safety_stat1_1_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat1_1_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-2">
            <h4>{{ $page->safety_stat1_2_text }}</h4>
            <img src="{{ $page->safety_stat1_2_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat1_2_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-3">
            <h4>{{ $page->safety_stat1_3_text }}</h4>
            <img src="{{ $page->safety_stat1_3_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat1_3_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
    </div>
</section>
<div id="trigger4" class="spacer s0"></div>
<section id="content-section-left" class="saftey">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-push-6 content-img-right">
                <div class="bkg-img" style="background-image: url('{{ $page->safety2_image[0] }}')"></div>
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->safety2_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="content-right">
                    {{ $page->safety2_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="stat-section">
    <div class="container">
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-4">
            <h4>{{ $page->safety_stat2_1_text }}</h4>
            <img src="{{ $page->safety_stat2_1_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat2_1_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-5">
            <h4>{{ $page->safety_stat2_2_text }}</h4>
            <img src="{{ $page->safety_stat2_2_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat2_2_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
        <div class="col-md-4 text-center view-chart" data-toggle="modal" data-target="#myModal-6">
            <h4>{{ $page->safety_stat2_3_text }}</h4>
            <img src="{{ $page->safety_stat2_3_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat2_3_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->
    </div>
</section>
<div id="trigger4" class="spacer s0"></div>
<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->safety3_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->safety3_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->safety3_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="stat-section">
    <div class="container">
        <div class="col-md-4 col-md-offset-4 text-center view-chart" data-toggle="modal" data-target="#myModal-7">
            <h4>{{ $page->safety_stat3_1_text }}</h4>
            <img src="{{ $page->safety_stat3_1_icon[0] }}" width="100">
            <h4>VIEW CHART</h4>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal-7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <img src="{{ $page->safety_stat3_1_chart[0] }}" width="700">
                </div>
            </div>
        </div>
        <!-- End Modal -->

    </div>
</section>




@stop