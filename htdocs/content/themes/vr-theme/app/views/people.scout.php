@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')

<div id="trigger3" class="spacer s0"></div>
<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->people1_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->people1_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->people1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center">
            <img src="{{ $page->people_stat1_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->people_stat1_1_number }}</span>+
            <h4>{{ $page->people_stat1_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->people_stat1_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->people_stat1_2_number }}</span>
            <h4>{{ $page->people_stat1_2_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->people_stat1_3_icon[0] }}" width="70">
            <span class="counter">{{ $page->people_stat1_3_number }}</span>
            <h4>{{ $page->people_stat1_3_text }}</h4>
        </div>
    </div>
</section>



@stop

