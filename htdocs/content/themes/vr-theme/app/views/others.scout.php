@extends('layouts.nocomm')

@section('content')

@include('inc/hero')




<? //Guiding Principles ?>

<section id="others">
    <div class="container">
        <div class="row">
            <div class="left-arrow"><i class="fa fa-angle-left fa-2"></i></div>
            <div class="next-arrow"><i class="fa fa-angle-right fa-2"></i></div>
            <div class="col-md-8 col-md-offset-2">
                <div class="other-slides responsive">

<!--                    <div>-->
<!--                        <h2 class="green-text slide-title">{{ $page->timeline_title1 }}</h2>-->
<!--                        {{ $page->timeline_content1 }}-->
<!--                    </div>-->
                    <div>
                        <h2 class="green-text slide-title">{{ $page->timeline_title2 }}</h2>
                        {{ $page->timeline_content2 }}
                    </div>
                    <div>
                        <h2 class="green-text slide-title">{{ $page->timeline_title3 }}</h2>
                        {{ $page->timeline_content3 }}
                    </div>
                    <div>
                        <h2 class="green-text slide-title">{{ $page->timeline_title4 }}</h2>
                        {{ $page->timeline_content4 }}
                    </div>
                    <div>
                        <h2 class="green-text slide-title">{{ $page->timeline_title5 }}</h2>
                        {{ $page->timeline_content5 }}
                    </div>
                    <div>
                        <h2 class="green-text slide-title">{{ $page->timeline_title6 }}</h2>
                        {{ $page->timeline_content6 }}
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@stop