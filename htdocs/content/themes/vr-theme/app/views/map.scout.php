
@extends('layouts.nocomm')

@section('content')

<div id="mapp-popup1" class="mapp-overlay">
    <div class="mapp-popup">
        <h2>Hover over regions of the map for more information</h2>
        <div class="mapp-content" style="background-image:url('{{ themosis_assets() }}/images/new-hover.gif')">
        </div>
<!--        <a class="mapp-close" href="#mapp-popup1">Thanks, got it!</a>-->
    </div>
</div>

<div id="map-hero">
    <section id="subpage-hero-map" style="background-image: url('{{ $page->hero_background[0] }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-2">
                    <h3>{{ $page->hero_title }}</h3>
                    <div class="quote-box">
                        {{ $page->hero_statement }}
                    </div>
                </div>

                <object id="map" class="map" data="{{ themosis_assets() }}/images/map.svg" type="image/svg+xml" width="100%" height="100%"></object>

                <!--<div id="map" class="map"></div> -->
                <div id="map-mobile" style="display:none">
                    <img src="{{ themosis_assets() }}/images/map-mobile.png" width="100%">
                </div>

                <div class="pop-up pop-up-western">
                    <h4 class="text-center">{{ $page->map_title1 }}</h4>
                    <img src="{{ $page->map_image1[0] }}" width="250"/>
                    <p>{{ $page->map_content1 }}</p>
                </div>

                <div class="pop-up pop-up-mountain">
                    <h4 class="text-center">{{ $page->map_title2 }}</h4>
                    <img src="{{ $page->map_image2[0] }}" width="250"/>
                    <p>{{ $page->map_content2 }}</p>
                </div>

                <div class="pop-up pop-up-southwest">
                    <h4 class="text-center">{{ $page->map_title3 }}</h4>
                    <img src="{{ $page->map_image3[0] }}" width="250"/>
                    <p>{{ $page->map_content3 }}</p>
                </div>

                <div class="pop-up pop-up-central">
                    <h4 class="text-center">{{ $page->map_title4 }}</h4>
                    <img src="{{ $page->map_image4[0] }}" width="250"/>
                    <p>{{ $page->map_content4 }}</p>
                </div>

                <div class="pop-up pop-up-southerngulf">
                    <h4 class="text-center">{{ $page->map_title5 }}</h4>
                    <img src="{{ $page->map_image5[0] }}" width="250"/>
                    <p>{{ $page->map_content5 }}</p>
                </div>

                <div class="pop-up pop-up-mideast">
                    <h4 class="text-center">{{ $page->map_title6 }}</h4>
                    <img src="{{ $page->map_image6[0] }}" width="250"/>
                    <p>{{ $page->map_content6 }}</p>
                </div>

                <div class="pop-up pop-up-southeast">
                    <h4 class="text-center">{{ $page->map_title7 }}</h4>
                    <img src="{{ $page->map_image7[0] }}" width="250"/>
                    <p>{{ $page->map_content7 }}</p>
                </div>

                <div class="pop-up pop-up-international">
                    <h4 class="text-center">{{ $page->map_title8 }}</h4>
                    <img src="{{ $page->map_image8[0] }}" width="250"/>
                    <p>{{ $page->map_content8 }}</p>
                </div>

            </div>
        </div>
    </section>
</div>

<div id="map-mobile-content" style="display: none">
    <div class="container-fluid">
        <div class="row">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m1" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title1 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image1[0] }}" width="100%">
                            <p>{{ $page->map_content1 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m2" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title2 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image2[0] }}" width="100%">
                            <p>{{ $page->map_content2 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m3" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title3 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image3[0] }}" width="100%">
                            <p>{{ $page->map_content3 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m4" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title4 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image4[0] }}" width="100%">
                            <p>{{ $page->map_content4 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m5" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title5 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image5[0] }}" width="100%">
                            <p>{{ $page->map_content5 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m6" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title6 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image6[0] }}" width="100%">
                            <p>{{ $page->map_content6 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m7" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title7 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image7[0] }}" width="100%">
                            <p>{{ $page->map_content7 }}</p>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#m8" aria-expanded="true" aria-controls="collapseOne">
                                {{ $page->map_title8 }}
                            </a>
                        </h4>
                    </div>
                    <div id="m8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $page->map_image8[0] }}" width="100%">
                            <p>{{ $page->map_content8 }}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center">
            <img src="{{ $page->map_stat1_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->map_stat1_1_number }}</span>
            <h4>{{ $page->map_stat1_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->map_stat1_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->map_stat1_2_number }}</span>
            <h4>{{ $page->map_stat1_2_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->map_stat1_3_icon[0] }}" width="70">
            <span class="counter">{{ $page->map_stat1_3_number }}</span>+
            <h4>{{ $page->map_stat1_3_text }}</h4>
        </div>
    </div>
</section>

@stop