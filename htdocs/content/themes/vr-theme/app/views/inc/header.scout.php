<nav id="menu" class="navbar navbar-fixed-top tk-nimbus-sans-condensed" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/content/uploads/2015/09/vulcan_materials_company-white.png" height="50" alt="Vulcan Materials Company"></a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-1">

            <?

            wp_nav_menu([
                'theme_location' => 'header-nav',
                'container' => 'false',
                'menu_class' => 'nav navbar-nav navbar-right'
            ]);

            ?>

        </div>
    </div>
</nav>




