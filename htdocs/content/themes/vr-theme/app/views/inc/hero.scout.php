<div id="parallax2" class="parallaxParent2">
    <section id="subpage-hero" style="background-image: url('{{ $page->hero_background[0] }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h3>{{ $page->hero_title }}</h3>
                    <div class="quote-box">
                        {{ $page->hero_statement }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="mobile-quote" style="display: none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="quote-box-mobile">
                    {{ $page->hero_statement }}
                </div>
            </div>
        </div>
    </div>
</div>




