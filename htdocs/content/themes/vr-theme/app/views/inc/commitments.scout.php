<div id="trigger2" class="spacer s0"></div>
<section id="commitments">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2 class="section-title green-text wide-title">{{ $commitments->commitments_title }}</h2>
            </div>
        </div>
        <div class="row">
            @foreach ( $commitments->commitment as $item )
            <div class="col-md-3 commit-img {{ $item['commitment_class'] }}">
                <img src="{{ $item['commitment_img'][0] }}" width="100%">
                <div class="commit-title" style="background:#{{ $item['commitment_color'] }}">
                    <div class="center-title">
                        <span class="text-center">{{ $item['commitment_title1'] }}</span>
                    </div>
                </div>
                <p>{{ nl2br($item['commitment_content1']) }}</p>
                <div class="commitment-button"><a href="{{ $item['commitment_button_link1'] }}">{{ $item['commitment_button_title1'] }}</a></div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<div id="commitments-tab-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <h2 class="section-title green-text wide-title">{{ $commitments->commitments_title }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach ( $commitments->commitment as $item )
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $item['commitment_color'] }}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $item['commitment_title1'] }}
                            </a>
                        </h4>
                    </div>
                    <div id="{{ $item['commitment_color'] }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <img src="{{ $item['commitment_img'][0] }}" width="100%">
                            <p>{{ nl2br($item['commitment_content1']) }}</p>
                            <div class="commitment-button"><a href="{{ $item['commitment_button_link1'] }}">{{ $item['commitment_button_title1'] }}</a></div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>