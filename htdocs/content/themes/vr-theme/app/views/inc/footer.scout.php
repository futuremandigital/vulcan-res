<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<a class="footer-brand" href="http://www.vulcanmaterials.com/" target="_blank"><img src="/content/uploads/2015/09/vulcan_materials_company-white.png" height="50" alt="Vulcan Materials Company"></a>

			</div>
				<div class="col-md-6 text-right">

					<?php echo do_shortcode('[shareaholic app="share_buttons" id="21676912"]'); ?>
					<button class="contact-button" data-toggle="modal" data-target="#footerModal">Contact Us</button>
					<a href="/2015report.pdf" class="download-report" download><img src="{{ themosis_assets() }}/images/vulcan-download-button.png" width="220"></a>
				</div>
			</div>
		</div>
</section>

<!-- Modal -->
<div class="modal fade" id="footerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Contact Us</h4>
			</div>
			<div class="modal-body">
				<?php echo do_shortcode('[gravityform id=1 ajax=true title=false description=false]'); ?>

			</div>

		</div>
	</div>
</div>




