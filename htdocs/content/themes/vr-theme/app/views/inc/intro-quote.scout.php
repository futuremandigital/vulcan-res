<section id="commit-intro-quote">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-center">{{ $page->intro_quote }}</h4>
                <p class="text-center">- {{ $page->quote_author }}</p>
            </div>
        </div>
    </div>
</section>


