@extends('layouts.main')

@section('content')
<div id="parallax2" class="parallaxParent2">
    <section id="subpage-hero" class="community-hero" style="background-image: url('{{ $page->hero_background[0] }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    <h3>{{ $page->hero_title }}</h3>
                    <div class="quote-box">
                        {{ $page->hero_statement }}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="mobile-quote" style="display:none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="quote-box-mobile">
                    {{ $page->hero_statement }}
                </div>
            </div>
        </div>
    </div>
</div>

@include('inc/intro-quote')
<div id="trigger3" class="spacer s0"></div>
<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->community1_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->community1_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->community1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 col-md-offset-2 text-center">
            <img src="{{ $page->community_stat1_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->community_stat1_1_number }}</span>
            <h4>{{ $page->community_stat1_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat1_2_icon[0] }}" width="70">
            $<span class="counter">{{ $page->community_stat1_2_number }}</span>
            <h4>{{ $page->community_stat1_2_text }}</h4>
        </div>

    </div>
</section>
<div id="trigger4" class="spacer s0"></div>
<section id="content-section-left" class="saftey">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-push-6 content-img-right">
                <div class="bkg-img" style="background-image: url('{{ $page->community2_image[0] }}')"></div>
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->community2_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="content-right">
                    {{ $page->community2_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat2_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->community_stat2_1_number }}</span>
            <h4>{{ $page->community_stat2_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat2_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->community_stat2_2_number }}</span>
            <h4>{{ $page->community_stat2_2_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat2_3_icon[0] }}" width="70">
            <span class="counter">{{ $page->community_stat2_3_number }}</span>
            <h4>{{ $page->community_stat2_3_text }}</h4>
        </div>
    </div>
</section>

<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->community3_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->community3_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->community3_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat3_1_icon[0] }}" width="70">
            $<span class="counter">{{ $page->community_stat3_1_number }}</span>M
            <h4>{{ $page->community_stat3_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->community_stat3_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->community_stat3_2_number }}</span>
            <h4>{{ $page->community_stat3_2_text }}</h4>
        </div>

        <div class="col-md-4 text-center ">
            <img src="{{ $page->community_stat3_3_icon[0] }}" width="70">
            $<span class="counter">{{ $page->community_stat3_3_number }}</span>M
            <h4>{{ $page->community_stat3_3_text }}</h4>
        </div>
    </div>
</section>






@stop