@extends('layouts.main')

@section('content')

@include('inc/hero')

@include('inc/intro-quote')
<div id="trigger3" class="spacer s0"></div>
<section id="content-section-right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 content-img-left" style="background-image: url('{{ $page->natural1_image[0] }}')" >
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->natural1_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-right">
                    {{ $page->natural1_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 text-center">
            <img src="{{ $page->natural_stat1_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->natural_stat1_1_number }}</span>
            <h4>{{ $page->natural_stat1_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->natural_stat1_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->natural_stat1_2_number }}</span>B
            <h4>{{ $page->natural_stat1_2_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->natural_stat1_3_icon[0] }}" width="70">
            <span class="counter">{{ $page->natural_stat1_3_number }}</span>
            <h4>{{ $page->natural_stat1_3_text }}</h4>
        </div>
    </div>
</section>
<div id="trigger4" class="spacer s0"></div>
<section id="content-section-left" class="saftey">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-push-6 content-img-right">
                <div class="bkg-img" style="background-image: url('{{ $page->natural2_image[0] }}')"></div>
                <div class="caption" style="color:#fff">
                    <div class="blur"></div>
                    <div class="caption-text">
                        <p>{{ $page->natural2_image_cap }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="content-right">
                    {{ $page->natural2_content }}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="number-ticker">
    <div class="container">
        <div class="col-md-4 col-md-offset-2 text-center">
            <img src="{{ $page->natural_stat2_1_icon[0] }}" width="70">
            <span class="counter">{{ $page->natural_stat2_1_number }}</span>
            <h4>{{ $page->natural_stat2_1_text }}</h4>
        </div>

        <div class="col-md-4 text-center">
            <img src="{{ $page->natural_stat2_2_icon[0] }}" width="70">
            <span class="counter">{{ $page->natural_stat2_2_number }}</span>M
            <h4>{{ $page->natural_stat2_2_text }}</h4>
        </div>

    </div>
</section>





@stop