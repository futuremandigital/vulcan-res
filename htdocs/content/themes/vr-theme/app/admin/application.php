<?php

/**
 * application.php - Write your custom code below.
 */

Asset::add('aegis-style', 'css/styles.min.css', false, '1.0', 'all');
Asset::add('aegis-header', 'js/header.min.js', false, '1.0', true);
Asset::add('aegis-vendor', 'js/app.min.js', false, '1.0', true);
