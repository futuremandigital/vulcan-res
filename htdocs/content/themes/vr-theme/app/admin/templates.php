<?

//Check for templates and create custom Meta for editing
if (!empty($_GET['post']))
{
    $template = Meta::get($_GET['post'], '_themosisPageTemplate');
}

//Save template metabox data
if (!empty($_POST['post_ID']))
{
    $template = Meta::get($_POST['post_ID'], '_themosisPageTemplate');
}

//Meta for every page except homepage
if (!empty($template))
{

    if ($template != 'homepage')
    {

        Metabox::make('Hero Element', 'page', [
            'context' => 'normal'
        ])->set([
            Field::media('hero-banner', ['title' => 'Hero Background']),
            Field::text('hero-title', ['title' => 'Hero Title']),
            Field::editor('hero-statement', ['title' => 'Hero Statement'])

        ]);

        Metabox::make('Header Content', 'page', [
            'context' => 'normal'
        ])->set([
            Field::text('intro-quote', ['title' => 'Quote']),
            Field::text('quote-author', ['title' => 'Author'])
        ]);


    }

    switch ($template){
        case 'homepage':

            Metabox::make('Hero Carousel', 'page', [
                'context'   => 'normal'
            ])->set(array(

                Field::infinite('carousel', [
                    Field::media('banner', ['title' => 'Slide Background']),
                    Field::text('slide_title', ['title' => 'Slide Title']),
                    Field::text('slide_subtitle', ['title' => 'Slide Sub Title']),
                ], [
                    'title' => 'Homepage Carousel',
                    'limit' => 10
                ])
            ));

            Metabox::make('Intro', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('intro_bg_img', ['title' => 'Background Image']),
                Field::text('quote_title', ['title' => 'Quote Title']),
                Field::editor('box_content', ['title' => 'Box Content']),
                Field::text('box_quote', ['title' => 'Box Quote']),
                Field::text('box_author', ['title' => 'Box Author']),
            ]);

            Metabox::make('Our Mission', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('mission_title', ['title' => 'Title']),
                Field::textarea('mission_content', ['title' => 'Content']),
            ]);


            Metabox::make('Guiding Principles', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('principles_title', ['title' => 'Title']),
                Field::infinite('principles_slider', [
                    Field::media('pslide_bg', ['title' => 'Slide Background']),
                    Field::text('pslide_title', ['title' => 'Slide Title']),
                    Field::textarea('pslide_subtitle', ['title' => 'Slide Sub Title']),
                    Field::text('pslide_id', ['title' => 'ID']),
                ], [
                    'title' => 'Principle Slider',
                    'limit' => 10
                ])
            ]);


            Metabox::make('Letter from CEO', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('ceo_title', ['title' => 'Title']),
                Field::editor('ceo_preview', ['title' => 'Preview Text']),
                Field::media('ceo_bg_img', ['title' => 'Background Image']),
                Field::editor('ceo_content', ['title' => 'Full Text']),
                Field::text('ceo_button', ['title' => 'Button Title']),
            ]);




            Metabox::make('Commitments', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::textarea('commitments_title', ['title' => 'Title']),
                Field::infinite('commitment',[
                    Field::media('commitment_img', ['title' => 'Image']),
                    Field::text('commitment_title1', ['title' => 'Title']),
                    Field::text('commitment_color', ['title' => 'Background Color']),
                    Field::text('commitment_class', ['title' => 'Section Class']),
                    Field::textarea('commitment_content1', ['title' => 'Content']),
                    Field::text('commitment_button_title1', ['title' => 'Button Title']),
                    Field::text('commitment_button_link1', ['title' => 'Button Link']),
                ], [
                    'title' => 'Commitment',
                    'limit' => 4
                ])
            ]);

        break;

        case 'safety';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety1-image', ['title' => 'Image']),
                Field::text('safety1-image-cap', ['title' => 'Image Caption']),
                Field::editor('safety1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety-stat1-1-icon', ['title' => 'Icon #1']),
                Field::text('safety-stat1-1-text', ['title' => 'Text #1']),
                Field::media('safety-stat1-1-chart', ['title' => 'Chart #1']),
                Field::media('safety-stat1-2-icon', ['title' => 'Icon #2']),
                Field::text('safety-stat1-2-text', ['title' => 'Text #2']),
                Field::media('safety-stat1-2-chart', ['title' => 'Chart #2']),
                Field::media('safety-stat1-3-icon', ['title' => 'Icon #3']),
                Field::text('safety-stat1-3-text', ['title' => 'Text #3']),
                Field::media('safety-stat1-3-chart', ['title' => 'Chart #3']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety2-image', ['title' => 'Image']),
                Field::text('safety2-image-cap', ['title' => 'Image Caption']),
                Field::editor('safety2-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety-stat2-1-icon', ['title' => 'Icon #1']),
                Field::text('safety-stat2-1-text', ['title' => 'Text #1']),
                Field::media('safety-stat2-1-chart', ['title' => 'Chart #1']),
                Field::media('safety-stat2-2-icon', ['title' => 'Icon #2']),
                Field::text('safety-stat2-2-text', ['title' => 'Text #2']),
                Field::media('safety-stat2-2-chart', ['title' => 'Chart #2']),
                Field::media('safety-stat2-3-icon', ['title' => 'Icon #3']),
                Field::text('safety-stat2-3-text', ['title' => 'Text #3']),
                Field::media('safety-stat2-3-chart', ['title' => 'Chart #3']),
            ]);

            Metabox::make('Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety3-image', ['title' => 'Image']),
                Field::text('safety3-image-cap', ['title' => 'Image Caption']),
                Field::editor('safety3-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('safety-stat3-1-icon', ['title' => 'Icon #1']),
                Field::text('safety-stat3-1-text', ['title' => 'Text #1']),
                Field::media('safety-stat3-1-chart', ['title' => 'Chart #1']),
            ]);

        break;

        case 'people';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('people1-image', ['title' => 'Image']),
                Field::text('people1-image-cap', ['title' => 'Image Caption']),
                Field::editor('people1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('people-stat1-1-icon', ['title' => 'Icon #1']),
                Field::text('people-stat1-1-number', ['title' => 'Number #1']),
                Field::text('people-stat1-1-text', ['title' => 'Text #1']),
                Field::media('people-stat1-2-icon', ['title' => 'Icon #2']),
                Field::text('people-stat1-2-number', ['title' => 'Number #2']),
                Field::text('people-stat1-2-text', ['title' => 'Text #2']),
                Field::media('people-stat1-3-icon', ['title' => 'Icon #3']),
                Field::text('people-stat1-3-number', ['title' => 'Number #3']),
                Field::text('people-stat1-3-text', ['title' => 'Text #3']),
            ]);

        break;

        case 'natural';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural1-image', ['title' => 'Image']),
                Field::text('natural1-image-cap', ['title' => 'Image Caption']),
                Field::editor('natural1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural-stat1-1-icon', ['title' => 'Icon #1']),
                Field::text('natural-stat1-1-number', ['title' => 'Number #1']),
                Field::text('natural-stat1-1-text', ['title' => 'Text #1']),
                Field::media('natural-stat1-2-icon', ['title' => 'Icon #2']),
                Field::text('natural-stat1-2-number', ['title' => 'Number #2']),
                Field::text('natural-stat1-2-text', ['title' => 'Text #2']),
                Field::media('natural-stat1-3-icon', ['title' => 'Icon #3']),
                Field::text('natural-stat1-3-number', ['title' => 'Number #3']),
                Field::text('natural-stat1-3-text', ['title' => 'Text #3']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural2-image', ['title' => 'Image']),
                Field::text('natural2-image-cap', ['title' => 'Image Caption']),
                Field::editor('natural2-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('natural-stat2-1-icon', ['title' => 'Icon #1']),
                Field::text('natural-stat2-1-number', ['title' => 'Number #1']),
                Field::text('natural-stat2-1-text', ['title' => 'Text #1']),
                Field::media('natural-stat2-2-icon', ['title' => 'Icon #2']),
                Field::text('natural-stat2-2-number', ['title' => 'Number #2']),
                Field::text('natural-stat2-2-text', ['title' => 'Text #2']),
            ]);

        break;

        case 'community';

            Metabox::make('Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community1-image', ['title' => 'Image']),
                Field::text('community1-image-cap', ['title' => 'Image Caption']),
                Field::editor('community1-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community-stat1-1-icon', ['title' => 'Icon #1']),
                Field::text('community-stat1-1-number', ['title' => 'Number #1']),
                Field::text('community-stat1-1-text', ['title' => 'Text #1']),
                Field::media('community-stat1-2-icon', ['title' => 'Icon #2']),
                Field::text('community-stat1-2-number', ['title' => 'Number #2']),
                Field::text('community-stat1-2-text', ['title' => 'Text #2']),
            ]);

            Metabox::make('Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community2-image', ['title' => 'Image']),
                Field::text('community2-image-cap', ['title' => 'Image Caption']),
                Field::editor('community2-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #2', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community-stat2-1-icon', ['title' => 'Icon #1']),
                Field::text('community-stat2-1-number', ['title' => 'Number #1']),
                Field::text('community-stat2-1-text', ['title' => 'Text #1']),
                Field::media('community-stat2-2-icon', ['title' => 'Icon #2']),
                Field::text('community-stat2-2-number', ['title' => 'Number #2']),
                Field::text('community-stat2-2-text', ['title' => 'Text #2']),
                Field::media('community-stat2-3-icon', ['title' => 'Icon #3']),
                Field::text('community-stat2-3-number', ['title' => 'Number #3']),
                Field::text('community-stat2-3-text', ['title' => 'Text #3']),
            ]);

            Metabox::make('Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community3-image', ['title' => 'Image']),
                Field::text('community3-image-cap', ['title' => 'Image Caption']),
                Field::editor('community3-content', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #3', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('community-stat3-1-icon', ['title' => 'Icon #1']),
                Field::text('community-stat3-1-number', ['title' => 'Number #1']),
                Field::text('community-stat3-1-text', ['title' => 'Text #1']),
                Field::media('community-stat3-2-icon', ['title' => 'Icon #2']),
                Field::text('community-stat3-2-number', ['title' => 'Number #2']),
                Field::text('community-stat3-2-text', ['title' => 'Text #2']),
                Field::media('community-stat3-3-icon', ['title' => 'Icon #3']),
                Field::text('community-stat3-3-number', ['title' => 'Number #3']),
                Field::text('community-stat3-3-text', ['title' => 'Text #3']),
            ]);

        break;

        case 'others';

            Metabox::make('Timeline Slider', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('timeline-title1', ['title' => 'Title']),
                Field::editor('timeline-content1', ['title' => 'Content']),
                Field::text('timeline-title2', ['title' => 'Title']),
                Field::editor('timeline-content2', ['title' => 'Content']),
                Field::text('timeline-title3', ['title' => 'Title']),
                Field::editor('timeline-content3', ['title' => 'Content']),
                Field::text('timeline-title4', ['title' => 'Title']),
                Field::editor('timeline-content4', ['title' => 'Content']),
                Field::text('timeline-title5', ['title' => 'Title']),
                Field::editor('timeline-content5', ['title' => 'Content']),
                Field::text('timeline-title6', ['title' => 'Title']),
                Field::editor('timeline-content6', ['title' => 'Content']),
            ]);

        break;

        case 'map';

            Metabox::make('Western Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title1', ['title' => 'title']),
                Field::media('map-image1', ['title' => 'Image']),
                Field::editor('map-content1', ['title' => 'Content']),
            ]);

            Metabox::make('Mountain West Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title2', ['title' => 'title']),
                Field::media('map-image2', ['title' => 'Image']),
                Field::editor('map-content2', ['title' => 'Content']),
            ]);

            Metabox::make('Southwest Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title3', ['title' => 'title']),
                Field::media('map-image3', ['title' => 'Image']),
                Field::editor('map-content3', ['title' => 'Content']),
            ]);

            Metabox::make('Central Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title4', ['title' => 'title']),
                Field::media('map-image4', ['title' => 'Image']),
                Field::editor('map-content4', ['title' => 'Content']),
            ]);

            Metabox::make('Southern & Gulf Coast Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title5', ['title' => 'title']),
                Field::media('map-image5', ['title' => 'Image']),
                Field::editor('map-content5', ['title' => 'Content']),
            ]);

            Metabox::make('Mideast Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title6', ['title' => 'title']),
                Field::media('map-image6', ['title' => 'Image']),
                Field::editor('map-content6', ['title' => 'Content']),
            ]);

            Metabox::make('Southeast Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title7', ['title' => 'title']),
                Field::media('map-image7', ['title' => 'Image']),
                Field::editor('map-content7', ['title' => 'Content']),
            ]);

            Metabox::make('International Division', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::text('map-title8', ['title' => 'title']),
                Field::media('map-image8', ['title' => 'Image']),
                Field::editor('map-content8', ['title' => 'Content']),
            ]);

            Metabox::make('Stat Section #1', 'page', [
                'context'   => 'normal'
            ])->set([
                Field::media('map-stat1-1-icon', ['title' => 'Icon #1']),
                Field::text('map-stat1-1-number', ['title' => 'Number #1']),
                Field::text('map-stat1-1-text', ['title' => 'Text #1']),
                Field::media('map-stat1-2-icon', ['title' => 'Icon #2']),
                Field::text('map-stat1-2-number', ['title' => 'Number #2']),
                Field::text('map-stat1-2-text', ['title' => 'Text #2']),
                Field::media('map-stat1-3-icon', ['title' => 'Icon #3']),
                Field::text('map-stat1-3-number', ['title' => 'Number #3']),
                Field::text('map-stat1-3-text', ['title' => 'Text #3']),
            ]);


    }

}