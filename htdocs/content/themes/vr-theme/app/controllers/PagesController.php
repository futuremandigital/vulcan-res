<?php

class PagesController extends BaseController
{

    public function __construct()
    {
        // Do something global here




    }


    //Homepage
    public function homepage()
    {

        $post_id = get_the_ID();

        $page = new stdClass();
        $carousel = Meta::get($post_id, 'carousel');

        foreach ($carousel as &$c) {
            $c['banner'] = wp_get_attachment_image_src($c['banner'], 'full');

        }

        $page->carousel = $carousel;

        // Intro
        $page->intro_bg_img = wp_get_attachment_image_src(Meta::get($post_id, 'intro_bg_img'), 'full');
        $page->quote_title = Meta::get($post_id, 'quote_title');
        $page->box_content = nl2br(Meta::get($post_id, 'box_content'));
        $page->box_quote = Meta::get($post_id, 'box_quote');
        $page->box_author = Meta::get($post_id, 'box_author');


        // Mission

        $page->mission_title = Meta::get($post_id, 'mission_title');
        $page->mission_content = nl2br(Meta::get($post_id, 'mission_content'));


        // Guiding Principles

        $page->principles_title = Meta::get($post_id, 'principles_title');

        $principles_slider = Meta::get($post_id, 'principles_slider');
        foreach ($principles_slider as &$p) {
            $p['pslide_bg'] = wp_get_attachment_image_src($p['pslide_bg'], 'full');
        }
        $page->principles_slider = $principles_slider;


        // Letter From the CEO

        $page->ceo_title = Meta::get($post_id, 'ceo_title');
        $page->ceo_preview = nl2br(Meta::get($post_id, 'ceo_preview'));
        $page->ceo_bg_img = wp_get_attachment_image_src(Meta::get($post_id, 'ceo_bg_img'), 'full');
        $page->ceo_content = apply_filters('the_content', Meta::get($post_id, 'ceo_content'));
        $page->ceo_button = Meta::get($post_id, 'ceo_button');


        // Commitments

        $page->commitments_title = nl2br(Meta::get($post_id, 'commitments_title'));

        $commitment = Meta::get($post_id, 'commitment');
        foreach ($commitment as &$t) {
            $t['commitment_img'] = wp_get_attachment_image_src($t['commitment_img'], 'full');
            $t['commitment_title'] = $t['commitment_title1'];
            $t['commitment_color'] = $t['commitment_color'];
            $t['commitment_class'] = $t['commitment_class'];
            $t['commitment_content'] = Meta::get($post_id, 'commitment_content1');
            $t['commitment_button_title'] = Meta::get($post_id, 'commitment_button_title1');
            $t['commitment_button_link'] = Meta::get($post_id, 'commitment_button_link1');
        }
        $page->commitment = $commitment;

        return View::make('homepage', compact('page'));
    }





    //Safety
    public function safety()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->safety1_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety1-image'), 'full');
        $page->safety1_image_cap = Meta::get($post_id, 'safety1-image-cap');
        $page->safety1_content = apply_filters('the_content', Meta::get($post_id, 'safety1-content'));
        $page->safety2_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety2-image'), 'full');
        $page->safety2_image_cap = Meta::get($post_id, 'safety2-image-cap');
        $page->safety2_content = apply_filters('the_content', Meta::get($post_id, 'safety2-content'));
        $page->safety3_image = wp_get_attachment_image_src(Meta::get($post_id, 'safety3-image'), 'full');
        $page->safety3_image_cap = Meta::get($post_id, 'safety3-image-cap');
        $page->safety3_content = apply_filters('the_content', Meta::get($post_id, 'safety3-content'));

        $page->safety_stat1_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-1-icon'), 'full');
        $page->safety_stat1_1_text = Meta::get($post_id, 'safety-stat1-1-text');
        $page->safety_stat1_1_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-1-chart'), 'full');
        $page->safety_stat1_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-2-icon'), 'full');
        $page->safety_stat1_2_text = Meta::get($post_id, 'safety-stat1-2-text');
        $page->safety_stat1_2_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-2-chart'), 'full');
        $page->safety_stat1_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-3-icon'), 'full');
        $page->safety_stat1_3_text = Meta::get($post_id, 'safety-stat1-3-text');
        $page->safety_stat1_3_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat1-3-chart'), 'full');

        $page->safety_stat2_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-1-icon'), 'full');
        $page->safety_stat2_1_text = Meta::get($post_id, 'safety-stat2-1-text');
        $page->safety_stat2_1_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-1-chart'), 'full');
        $page->safety_stat2_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-2-icon'), 'full');
        $page->safety_stat2_2_text = Meta::get($post_id, 'safety-stat2-2-text');
        $page->safety_stat2_2_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-2-chart'), 'full');
        $page->safety_stat2_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-3-icon'), 'full');
        $page->safety_stat2_3_text = Meta::get($post_id, 'safety-stat2-3-text');
        $page->safety_stat2_3_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat2-3-chart'), 'full');

        $page->safety_stat3_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat3-1-icon'), 'full');
        $page->safety_stat3_1_text = Meta::get($post_id, 'safety-stat3-1-text');
        $page->safety_stat3_1_chart = wp_get_attachment_image_src(Meta::get($post_id, 'safety-stat3-1-chart'), 'full');





        return View::make('safety', compact('page'));
    }

    //People
    public function people()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->people1_image = wp_get_attachment_image_src(Meta::get($post_id, 'people1-image'), 'full');
        $page->people1_image_cap = Meta::get($post_id, 'people1-image-cap');
        $page->people1_content = apply_filters('the_content', Meta::get($post_id, 'people1-content'));

        $page->people_stat1_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'people-stat1-1-icon'), 'full');
        $page->people_stat1_1_number = Meta::get($post_id, 'people-stat1-1-number');
        $page->people_stat1_1_text = Meta::get($post_id, 'people-stat1-1-text');
        $page->people_stat1_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'people-stat1-2-icon'), 'full');
        $page->people_stat1_2_number = Meta::get($post_id, 'people-stat1-2-number');
        $page->people_stat1_2_text = Meta::get($post_id, 'people-stat1-2-text');
        $page->people_stat1_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'people-stat1-3-icon'), 'full');
        $page->people_stat1_3_number = Meta::get($post_id, 'people-stat1-3-number');
        $page->people_stat1_3_text = Meta::get($post_id, 'people-stat1-3-text');

        return View::make('people', compact('page'));
    }

    //Natural
    public function natural()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->natural1_image = wp_get_attachment_image_src(Meta::get($post_id, 'natural1-image'), 'full');
        $page->natural1_image_cap = Meta::get($post_id, 'natural1-image-cap');
        $page->natural1_content = apply_filters('the_content', Meta::get($post_id, 'natural1-content'));
        $page->natural2_image = wp_get_attachment_image_src(Meta::get($post_id, 'natural2-image'), 'full');
        $page->natural2_image_cap = Meta::get($post_id, 'natural2-image-cap');
        $page->natural2_content = apply_filters('the_content', Meta::get($post_id, 'natural2-content'));

        $page->natural_stat1_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'natural-stat1-1-icon'), 'full');
        $page->natural_stat1_1_number = Meta::get($post_id, 'natural-stat1-1-number');
        $page->natural_stat1_1_text = Meta::get($post_id, 'natural-stat1-1-text');
        $page->natural_stat1_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'natural-stat1-2-icon'), 'full');
        $page->natural_stat1_2_number = Meta::get($post_id, 'natural-stat1-2-number');
        $page->natural_stat1_2_text = Meta::get($post_id, 'natural-stat1-2-text');
        $page->natural_stat1_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'natural-stat1-3-icon'), 'full');
        $page->natural_stat1_3_number = Meta::get($post_id, 'natural-stat1-3-number');
        $page->natural_stat1_3_text = Meta::get($post_id, 'natural-stat1-3-text');

        $page->natural_stat2_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'natural-stat2-1-icon'), 'full');
        $page->natural_stat2_1_number = Meta::get($post_id, 'natural-stat2-1-number');
        $page->natural_stat2_1_text = Meta::get($post_id, 'natural-stat2-1-text');
        $page->natural_stat2_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'natural-stat2-2-icon'), 'full');
        $page->natural_stat2_2_number = Meta::get($post_id, 'natural-stat2-2-number');
        $page->natural_stat2_2_text = Meta::get($post_id, 'natural-stat2-2-text');



        return View::make('natural', compact('page'));
    }

    //Community
    public function community()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->community1_image = wp_get_attachment_image_src(Meta::get($post_id, 'community1-image'), 'full');
        $page->community1_image_cap = Meta::get($post_id, 'community1-image-cap');
        $page->community1_content = apply_filters('the_content', Meta::get($post_id, 'community1-content'));
        $page->community2_image = wp_get_attachment_image_src(Meta::get($post_id, 'community2-image'), 'full');
        $page->community2_image_cap = Meta::get($post_id, 'community2-image-cap');
        $page->community2_content = apply_filters('the_content', Meta::get($post_id, 'community2-content'));
        $page->community3_image = wp_get_attachment_image_src(Meta::get($post_id, 'community3-image'), 'full');
        $page->community3_image_cap = Meta::get($post_id, 'community3-image-cap');
        $page->community3_content = apply_filters('the_content', Meta::get($post_id, 'community3-content'));

        $page->community_stat1_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat1-1-icon'), 'full');
        $page->community_stat1_1_number = Meta::get($post_id, 'community-stat1-1-number');
        $page->community_stat1_1_text = Meta::get($post_id, 'community-stat1-1-text');
        $page->community_stat1_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat1-2-icon'), 'full');
        $page->community_stat1_2_number = Meta::get($post_id, 'community-stat1-2-number');
        $page->community_stat1_2_text = Meta::get($post_id, 'community-stat1-2-text');

        $page->community_stat2_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat2-1-icon'), 'full');
        $page->community_stat2_1_number = Meta::get($post_id, 'community-stat2-1-number');
        $page->community_stat2_1_text = Meta::get($post_id, 'community-stat2-1-text');
        $page->community_stat2_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat2-2-icon'), 'full');
        $page->community_stat2_2_number = Meta::get($post_id, 'community-stat2-2-number');
        $page->community_stat2_2_text = Meta::get($post_id, 'community-stat2-2-text');
        $page->community_stat2_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat2-3-icon'), 'full');
        $page->community_stat2_3_number = Meta::get($post_id, 'community-stat2-3-number');
        $page->community_stat2_3_text = Meta::get($post_id, 'community-stat2-3-text');

        $page->community_stat3_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat3-1-icon'), 'full');
        $page->community_stat3_1_number = Meta::get($post_id, 'community-stat3-1-number');
        $page->community_stat3_1_text = Meta::get($post_id, 'community-stat3-1-text');
        $page->community_stat3_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat3-2-icon'), 'full');
        $page->community_stat3_2_number = Meta::get($post_id, 'community-stat3-2-number');
        $page->community_stat3_2_text = Meta::get($post_id, 'community-stat3-2-text');
        $page->community_stat3_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'community-stat3-3-icon'), 'full');
        $page->community_stat3_3_number = Meta::get($post_id, 'community-stat3-3-number');
        $page->community_stat3_3_text = Meta::get($post_id, 'community-stat3-3-text');



        return View::make('community', compact('page'));
    }

    //Others
    public function others()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->timeline_title1 = Meta::get($post_id, 'timeline-title1');
        $page->timeline_content1 = Meta::get($post_id, 'timeline-content1');

        $page->timeline_title2 = Meta::get($post_id, 'timeline-title2');
        $page->timeline_content2 = Meta::get($post_id, 'timeline-content2');

        $page->timeline_title3 = Meta::get($post_id, 'timeline-title3');
        $page->timeline_content3 = Meta::get($post_id, 'timeline-content3');

        $page->timeline_title4 = Meta::get($post_id, 'timeline-title4');
        $page->timeline_content4 = Meta::get($post_id, 'timeline-content4');

        $page->timeline_title5 = Meta::get($post_id, 'timeline-title5');
        $page->timeline_content5 = Meta::get($post_id, 'timeline-content5');

        $page->timeline_title6 = Meta::get($post_id, 'timeline-title6');
        $page->timeline_content6 = Meta::get($post_id, 'timeline-content6');

        return View::make('others', compact('page'));
    }

    //Map
    public function map()
    {
        $post_id = get_the_ID();

        $page = $this->get_header($post_id);

        $page->map_stat1_1_icon = wp_get_attachment_image_src(Meta::get($post_id, 'map-stat1-1-icon'), 'full');
        $page->map_stat1_1_number = Meta::get($post_id, 'map-stat1-1-number');
        $page->map_stat1_1_text = Meta::get($post_id, 'map-stat1-1-text');
        $page->map_stat1_2_icon = wp_get_attachment_image_src(Meta::get($post_id, 'map-stat1-2-icon'), 'full');
        $page->map_stat1_2_number = Meta::get($post_id, 'map-stat1-2-number');
        $page->map_stat1_2_text = Meta::get($post_id, 'map-stat1-2-text');
        $page->map_stat1_3_icon = wp_get_attachment_image_src(Meta::get($post_id, 'map-stat1-3-icon'), 'full');
        $page->map_stat1_3_number = Meta::get($post_id, 'map-stat1-3-number');
        $page->map_stat1_3_text = Meta::get($post_id, 'map-stat1-3-text');

        $page->map_title1 = Meta::get($post_id, 'map-title1');
        $page->map_image1 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image1'), 'full');
        $page->map_content1 = Meta::get($post_id, 'map-content1');

        $page->map_title2 = Meta::get($post_id, 'map-title2');
        $page->map_image2 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image2'), 'full');
        $page->map_content2 = Meta::get($post_id, 'map-content2');

        $page->map_title3 = Meta::get($post_id, 'map-title3');
        $page->map_image3 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image3'), 'full');
        $page->map_content3 = Meta::get($post_id, 'map-content3');

        $page->map_title4 = Meta::get($post_id, 'map-title4');
        $page->map_image4 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image4'), 'full');
        $page->map_content4 = Meta::get($post_id, 'map-content4');

        $page->map_title5 = Meta::get($post_id, 'map-title5');
        $page->map_image5 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image5'), 'full');
        $page->map_content5 = Meta::get($post_id, 'map-content5');

        $page->map_title6 = Meta::get($post_id, 'map-title6');
        $page->map_image6 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image6'), 'full');
        $page->map_content6 = Meta::get($post_id, 'map-content6');

        $page->map_title7 = Meta::get($post_id, 'map-title7');
        $page->map_image7 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image7'), 'full');
        $page->map_content7 = Meta::get($post_id, 'map-content7');

        $page->map_title8 = Meta::get($post_id, 'map-title8');
        $page->map_image8 = wp_get_attachment_image_src(Meta::get($post_id, 'map-image8'), 'full');
        $page->map_content8 = Meta::get($post_id, 'map-content8');

        return View::make('map', compact('page'));
    }

    private function get_header($post_id)
    {


        $header = new stdClass();
        $header->hero_background = wp_get_attachment_image_src(Meta::get($post_id, 'hero-banner'), 'full');
        $header->hero_title = nl2br(Meta::get($post_id, 'hero-title'));
        $header->hero_statement = nl2br(Meta::get($post_id, 'hero-statement'));

        $header->intro_quote = nl2br(Meta::get($post_id, 'intro-quote'));
        $header->quote_author = nl2br(Meta::get($post_id, 'quote-author'));

        return $header;
    }




}